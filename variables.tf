variable "rg_name" {
  type = string
  description = "Variable to set resource group name"
}
variable "location" {
  type = string
  description = "Variable to set resource location"
  default = "West US"
}
