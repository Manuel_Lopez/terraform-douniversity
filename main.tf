# Terraform main in gitlab

terraform {
    required_providers {
        azurerm = {
            source = "hashicorp/azurerm"
            version = "2.46.0"
        }
    }
}

# Configure Azure provider
provider "azurerm" {
  features {}
}

# Creates a Virtual Network within a resourcegroup
resource "azurerm_virtual_network" "example" {
  name = "vn-terraform"
  resource_group_name = var.rg_name
  location = var.location
  address_space = [ "10.0.0.0/16" ]
}

# Creates a Subnet for a Virtual Network
resource "azurerm_subnet" "example" {
  name = "subNet-terraform"
  resource_group_name = var.rg_name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes = [ "10.0.0.0/24" ]
}

# this is the terramorm main file
